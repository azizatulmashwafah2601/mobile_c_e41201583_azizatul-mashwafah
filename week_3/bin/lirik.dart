import 'package:week_3/week_3.dart' as week_3;

void main(List<String> args) async{
  print("Ready. Sing");
  print(await line());
  print(await line2());
  print(await line3());
  print(await line4());
}

Future<String> line() async {
  String lyrics = "Tutur batinku tak akan salah";
  return await Future.delayed(Duration(seconds: 5), () => (lyrics));
}
Future<String> line2() async {
  String lyrics = "Silakan pergi, 'ku tak rasa kalah";
  return await Future.delayed(Duration(seconds: 4), () => (lyrics));
}
Future<String> line3() async {
  String lyrics = "Namun percayalah sejauh mana kau mencari";
  return await Future.delayed(Duration(seconds: 6), () => (lyrics));
}
Future<String> line4() async {
  String lyrics = "Takkan kau temukan yang sebaik ini";
  return await Future.delayed(Duration(seconds: 7), () => (lyrics));
}
