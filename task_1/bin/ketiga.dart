import 'dart:io';

import 'package:task_1/task_1.dart' as task_1;

void main(List<String> args) {
  stdout.write("Masukkan Nama Depan : ");
  String firstName = stdin.readLineSync()!;
  stdout.write("Masukkan Nama Belakang : ");
  String lastName = stdin.readLineSync()!;

  print("Nama Lengkap Anda Adalah $firstName $lastName");
}