import 'package:week_2/week_2.dart' as week_2;
import 'dart:io';

void main(List<String> args) {
  stdout.write("Masukkan Nama Depan : ");
  String firstName = stdin.readLineSync()!;
  stdout.write("Masukkan Nama Belakang : ");
  String lastName = stdin.readLineSync()!;

  print("Nama Lengkap Anda Adalah $firstName $lastName");
}