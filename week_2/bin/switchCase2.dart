import 'package:week_2/week_2.dart' as week_2;

void main(List<String> args) {
  var hari = 21;
  var bulan = 4;
  var tahun = 2021;

  switch (bulan) {
    case 1: print('$hari Januari $tahun');
      break;
    case 2: print('$hari Februari $tahun');
      break;
    case 3: print('$hari Maret $tahun');
      break;
    case 4: print('$hari April $tahun');
      break;
    default: (print('Nama bulan tidak ada'));
  }
}