import 'package:week_2/week_2.dart' as week_2;
import 'dart:io';

void main(List<String> args) {
  stdout.write("Input Nama : ");
  String nama = stdin.readLineSync()!;
  stdout.write("Input Peran : ");
  String peran = stdin.readLineSync()!;

  if (nama == '') {
    print('Nama harus diisi');
  } else if (peran == '') {
    print('Halo $nama, Pilih peranmu untuk memulai game');
  } else if (peran == 'Penyihir') {
    print('Selamat datang di Dunia Werewolf, $nama.' + ' Halo $peran $nama, kamu dapat melihat siapa yang menjadi werewolf');
  } else if (peran == 'Guard') {
    print('Selamat datang di Dunia Werewolf, $nama.' + ' Halo $peran $nama, kamu akan membantu melindungi temanmu dari serangan werewolf');
  } else if (peran == 'Werewolf') {
    print('Selamat datang di Dunia Werewolf, $nama.' + ' Halo $peran $nama, kamu akan memakan mangsa setiap malam!');
  } else {
    print('Error!');
  }
}