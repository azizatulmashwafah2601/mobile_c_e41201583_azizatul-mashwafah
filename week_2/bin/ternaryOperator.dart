import 'package:week_2/week_2.dart' as week_2;
import 'dart:io';
void main(List<String> args) {
  print('Apakah Anda akan menginstall aplikasi Dart?');
  stdout.write('Y/T ');
  String jawab = stdin.readLineSync()!;

  var hasil = (jawab == 'y')
    ? print('Anda akan menginstall aplikasi Dart')
    : print('Aborted');
}