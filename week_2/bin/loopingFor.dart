import 'package:test/test.dart';
import 'package:week_2/week_2.dart' as week_2;

void main(List<String> args) {
  for (int i = 1; i <= 20; i++) {
    if (i % 3 == 0 && i % 2 == 1) {
      print('$i - I Love Coding');
    } else if (i % 2 == 0) {
      print('$i - Berkualitas');
    } else if (i % 2 == 1) {
      print('$i - Santai');
    }
  }
}