import 'dart:ui';

class ChartModel {
  final String? name;
  final String? message;
  final String? time;
  final String? profileUrl;

  ChartModel({this.name, this.message, this.time, this.profileUrl});
}

final List<ChartModel> items = [
  ChartModel(
      name: 'hisna',
      message: 'Hello Hisna',
      time: '12.00',
      profileUrl:
          'https://akcdn.detik.net.id/community/media/visual/2021/05/17/fadil-jaidi_169.jpeg?w=700&q=90'),
  ChartModel(
      name: 'riska',
      message: 'hello Riska',
      time: '9 march',
      profileUrl:
          'https://asset.kompas.com/crops/IjmmRwybxe-4tZrGRN5eMrq_po8=/0x32:700x382/750x500/data/photo/2020/01/09/5e16811892fc7.jpg'),
  ChartModel(
      name: 'denny',
      message: 'hello Denny',
      time: '12.21',
      profileUrl:
          'https://assets.pikiran-rakyat.com/crop/0x0:0x0/x/photo/2021/07/26/1959788038.jpg'),
  ChartModel(
      name: 'vita',
      message: 'hello Vita',
      time: '10 march',
      profileUrl:
          'https://asset.kompas.com/crops/ployX7cQOqsYqJS2PYvUGv41CaI=/0x0:1000x667/750x500/data/photo/2017/06/22/163146320170622-042902-8311-chef.juna-.atau-.junior-.rorimpandey-.jpg'),
  ChartModel(
      name: 'rini',
      message: 'hello Rini',
      time: '12.21',
      profileUrl:
          'https://asset.kompas.com/crops/IjmmRwybxe-4tZrGRN5eMrq_po8=/0x32:700x382/750x500/data/photo/2020/01/09/5e16811892fc7.jpg'),
  ChartModel(
      name: 'fadil',
      message: 'hello Fadil',
      time: '13.41',
      profileUrl:
          'https://akcdn.detik.net.id/community/media/visual/2021/05/17/fadil-jaidi_169.jpeg?w=700&q=90'),
  ChartModel(
      name: 'yislam',
      message: 'hello Yislam',
      time: '25 january',
      profileUrl:
          'https://assets.pikiran-rakyat.com/crop/0x0:0x0/x/photo/2021/12/22/2020498917.jpeg'),
  ChartModel(
      name: 'taufik',
      message: 'hello Taufik',
      time: '19.00',
      profileUrl:
          'https://akcdn.detik.net.id/community/media/visual/2021/04/19/thariq-halilintar_169.jpeg?w=700&q=90'),
];