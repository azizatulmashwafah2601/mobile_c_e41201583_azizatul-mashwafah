import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Widget',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Widget Core Component I'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Column(children: <Widget>[
        Padding(padding: EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 0),
          child: Column(children: <Widget>[
            Text("Nasi + Mie Rebus"),
            Text("Nasi + Mie Rebus + Telor"),
            Text("Nasi + Ayam"),
            Text("Nasi + Ayam + Es Jeruk"),
            Text("Nasi + Ikan + Teh Manis"),
            Text("Nasi + Sate Ayam"),
          ],
          ),
        ),

        Padding(padding: EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 0),
          child: Column(children: <Widget>[
            Text('Ini Text', style: TextStyle(
              backgroundColor: Colors.pink,
              color: Colors.blue,
              fontSize: 20.0,
              fontStyle: FontStyle.italic,
              fontWeight: FontWeight.bold 
              ),
            ),
          ],
          ),
        ),

        Padding(padding: EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Icon(Icons.access_alarm),
                  Text('Alarm')
                ],
              ),
              Column(
                children: <Widget>[
                  Icon(Icons.phone),
                  Text('Phone')
                ],
              ),
              Column(
                children: <Widget>[
                  Icon(Icons.book),
                  Text('Book')
                ],
              ),
            ],
          ),
        ),

        Container(
          padding: EdgeInsets.all(32.0),
          margin: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20.0),
            color: Colors.purple
          ),
          child: Text('Haiii', style: TextStyle(color: Colors.white, fontSize: 20.0),
        ),
        ),

        Padding(padding: EdgeInsets.fromLTRB(8.0, 10.0, 8.0, 0),
          child: Column(children: <Widget>[
              RaisedButton(
                color: Colors.amber,
                child: Text("Raised Button"),
                onPressed: () {},
                ),
              Padding(padding: EdgeInsets.fromLTRB(8.0, 5.0, 8.0, 0),
              child: Column(children: <Widget>[
                MaterialButton(
                color: Colors.lime,
                child: Text("Material Button"),
                onPressed: () {},
                ),
              ],
              ),
              ),
              Padding(padding: EdgeInsets.fromLTRB(8.0, 5.0, 8.0, 0),
              child: Column(children: <Widget>[
                FlatButton(
                color: Colors.lightGreenAccent,
                child: Text("FlatButton Button"),
                onPressed: () {},
                ),
              ],
              ),
              ),
          ],
          ),
        ),

        Padding(padding: EdgeInsets.fromLTRB(0, 10.0, 0, 0),
          child: Column(children: <Widget>[
            TextFormField(
              decoration: InputDecoration(hintText: "Username"),
              keyboardType: TextInputType.emailAddress,
            ),
            Padding(padding: EdgeInsets.fromLTRB(0, 5.0, 0, 0),
              child: Column(children: <Widget>[
                TextFormField(
                  obscureText: true,
                  decoration: InputDecoration(hintText: "Password"),
                  keyboardType: TextInputType.visiblePassword,
                ),
              ],
              ),
            ),
            Padding(padding: EdgeInsets.fromLTRB(0, 8.0, 0, 5.0),
              child: Column(children: <Widget>[
                TextButton(
                    child: Text("Login",
                      style: TextStyle(
                        color: Colors.white
                      ),
                    ),
                    style: TextButton.styleFrom(
                      backgroundColor: Colors.blue
                    ),
                    onPressed: () {},
                  )
              ],
              ),
            ),
          ],
          ),
        ),
      ],
      ),
    );
  }
}
